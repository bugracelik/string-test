package com.bugra.app;

import org.junit.jupiter.api.Test;

import java.io.UnsupportedEncodingException;

//@SpringBootTest
class StringTestApplicationTests {

    @Test
    void deneme1() {
        char data[] = {'a', 'b', 'c'};
        String str = new String(data);
        System.out.println(str);
    }

    @Test
    void deneme2() {
        String cde = "cdefklm";
        System.out.println("abc" + cde);
        String substring = cde.substring(2, 4); //2 dahil 4 dahil degil
        System.out.println(substring);
    }

    @Test
    void deneme3() {
        String myStringValue = "abcdeeklbugracelik12dasd";
        System.out.println(myStringValue.charAt(0)); //Returns the char value at the specified index.
        System.out.println(myStringValue.charAt(1));
        System.out.println(myStringValue.charAt(2));
        System.out.println(myStringValue.charAt(3));
    }

    @Test
    void deneme4() {
        String myStringValue = "ABabcdeeklbugracelik12dasd";
        System.out.println(myStringValue.codePointAt(0));//unicode code point
        System.out.println(myStringValue.codePointAt(1));
    }

    @Test
    void deneme5() {
        String myStringValue1 = "ABabcdeeklbugracelik12dewqeqweqweqweqweqweasd1214112442141212412354";
        String myStringValue2 = "ABabcdeeklbugracelik12dasd";
        String myStringValue = "abcdeeklbugracelikdqwqdw12dasd";
        int i = myStringValue1.compareTo(myStringValue); //Compares two strings lexicographically.
        System.out.println(i);
    }

    @Test
    void deneme6() {
        String str = "bugra ";
        String str1 = "celik";
        System.out.println(str.concat(str1));//Concatenates the specified string to the end of this string.
    }

    @Test
    void deneme7() {
        String myStringValue = "ABabcdeeklbugracelik12dasd";
        System.out.println(myStringValue.contains("a"));
        System.out.println(myStringValue.contains("z"));
        //Returns true if and only if this string contains the specified sequence of char values.
    }

    @Test
    void deneme8() {
        String myStringValue = "ABabcdeeklbugracelik12dasd";
        String myStringValue1 = "ABabcdeeklbugracelik12dasdd";
        String myStringValue2 = "ABabcdeeklbugracelik12dasd";
        //Compares this string to the specified CharSequence.
        System.out.println(myStringValue.contentEquals(myStringValue1));
        System.out.println(myStringValue.contentEquals(myStringValue2));
    }

    @Test
    void deneme9() {
        String myStringValue = "ABabcdeeklbugracelik12dasd";
        char data[] = {'a', 'b', 'c'};
        String s = String.copyValueOf(data);
        System.out.println(s);
    }

    @Test
    void deneme10() {
        String myStringValue = "ABabcdeeklbugracelik12dasd";
        System.out.println(myStringValue.endsWith("d"));
        System.out.println(myStringValue.startsWith("A"));
    }

    @Test
    void deneme11() {
        String myStringValue1 = "ABabcdeeklbugracelik12dasd";
        String myStringValue2 = "ABabcdeEklbugracelik12dasd";
        String myStringValue3 = "ABabcdeeklbugracelik12dasd123124";
        //Compares this string to the specified object.
        System.out.println(myStringValue1.equals(myStringValue2));
        System.out.println(myStringValue1.equalsIgnoreCase(myStringValue2));
        System.out.println(myStringValue1.equals(myStringValue3));
    }

    @Test
    void deneme12() {
        //Returns a formatted string using the specified format string and arguments.
        String format = String.format("merhaba dünya %d, %.50f, %s", 10, 12.4, "bugra");
        System.out.println(format);
    }

    @Test
    void deneme13() {
        //Encodes this String into a sequence of bytes using the platform's default charset, storing the result into a new byte array.
        String myString = "ABCabcde";
        byte[] bytes = myString.getBytes();
        byte[] clone = bytes.clone();
        int length = bytes.length;
        int i = bytes.hashCode();

        for (Byte by : bytes)
            System.out.println(by);
    }

    @Test
    void deneme14() throws UnsupportedEncodingException {
        String myString = "ABCabcde";
        String myString1 = "ABCabcde";
        String myString2 = "ABCabcdqwee";
        System.out.println(myString.hashCode());
        System.out.println(myString1.hashCode());
        System.out.println(myString2.hashCode());
    }

    @Test
    void deneme15() {
        String myString = "ABCabcde";
        System.out.println(myString.indexOf(65));
        System.out.println(myString.indexOf("asd"));
        System.out.println(myString.indexOf("ABCabcde"));
    }

    @Test
    void deneme16() {
        String s1 = new String("hello");
        String s2 = "hello";
        String s3 = s1.intern();//returns string from pool, now it will be same as s2
        System.out.println(s1 == s2);//false because reference variables are pointing to different instance
        System.out.println(s2 == s3);//true because reference variables are pointing to same instance
    }

    @Test
    void deneme17() {
        String s1 = "Javatpoint";
        String s2 = s1.intern();
        String s3 = new String("Javatpoint");
        String s4 = s3.intern();
        System.out.println(s1==s2); // True
        System.out.println(s1==s3); // False
        System.out.println(s1==s4); // True
        System.out.println(s2==s3); // False
        System.out.println(s2==s4); // True
        System.out.println(s3==s4); // False
    }

    @Test
    void deneme18() {
        String selam = "selam";
    }
}
